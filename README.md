# Algorithms and Data Structures
Coursework for the Year 2 module Algorithms and Data Structures taught by Dr. Vasileios Koutavas and Dr. Ivana Dusparic.

## Exercises
### Semester 1
- [Arith](semester_1/Arith)
- [BST](semester_1/BST)
- [Collinear](semester_1/Collinear)
- [DoublyLinkedList](semester_1/DoublyLinkedList)
### Semester 2
- [Competition](semester_2/Competition)
- [SortComparison](semester_2/SortComparison)
