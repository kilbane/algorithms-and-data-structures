# Competition
A Contest to Meet (ACM) is a reality TV contest that sets three contestants at
three random city intersections. In order to win, the three contestants need
all to meet at any intersection of the city as fast as possible. The
contestants may arrive at the intersections at different times, in which case,
the first to arrive can wait until the others arrive. From an estimated
walking speed for each one of the three contestants, ACM wants to determine the
minimum time that a live TV broadcast should last to cover their journey
regardless of the contestants' initial positions and the intersection at which
they finally meet. Each contestant walks at a given estimated speed. The city
is a collection of intersections in which some pairs are connected by one-way
streets that the contestants can use to traverse the city. Two intersections
can be connected by two one-way streets allowing travel in opposite directions
of each other. 

The aims of this exercise were to:
- Answer the question using Dijkstra's algorithm
- Answer the question using the Floyd-Warshall algorithm
- Ensure 100% code coverage through unit tests

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

With no test failures, the only output will be four FileNotFoundException stack
traces.
