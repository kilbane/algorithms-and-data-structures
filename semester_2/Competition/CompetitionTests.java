// @Author: Neil Kilbane 
//
// Discussion: I have implemented solutions to this problem using both Dijkstra's algorithm and the Floyd-Warshall algorithm.
//
//            The Dijkstra algorithm has O(N^2) running time, where N is the number of nodes of the graph. This algorithm
//            calculates the shortest path from a single source node to every other node in the graph. Therefore, it is
//            necessary to run the algorithm once for every node in the graph, giving a running time of O(N^3) for the
//            solution. The Dijkstra solution implements the graph using a HashMap of HashMaps, with the key of the outer
//            map being the source node of an edge, and the value of the outer map being a HashMap whose key is the
//            destination node and whose value is the weight of the edge. In the worst case scenario, every node will be
//            connected to every other node by an edge, so the number of HashMaps will be N + N^2. The solution also requires
//            an array for maintaining the distance for each path from a source node. The solution uses O(N^2) memory space.
//
//            The Floyd-Warshall algorithm has O(N^3) running time, but it only needs to be implemented once because it finds
//            the shortest path from every node to every other node. So the solution has O(N^3) running time, which is the same 
//            as the Dijkstra solution. It also uses two NxN matrices, one for the graph, and one for the distances between each
//            pair of nodes. The solution uses O(N^2) memory space.
//
//            Although the order of growth for both solutions is the same with regards to both running time and memory space, the
//            Floyd-Warshall solution is more efficient than the Dijkstra solution because the growth constant is smaller.
//
// Extra files: impossibleEWD.txt - represents a graph where there is not a path from every node to every other node
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CompetitionTests {

    @Test
    public void testDijkstraConstructor() {
      CompetitionDijkstra tinyCompetition = new CompetitionDijkstra("tinyEWD.txt", 80, 70, 60);
      CompetitionDijkstra bigCompetition = new CompetitionDijkstra("1000EWD.txt", 80, 70, 60);
      CompetitionDijkstra impossibleCompetition = new CompetitionDijkstra("impossibleEWD.txt", 80, 70, 60);
      CompetitionDijkstra missingFileCompetition = new CompetitionDijkstra("missingEWD.txt", 80, 70, 60);
      CompetitionDijkstra badSpeedCompetition = new CompetitionDijkstra("tinyEWD.txt", 1000, 70, 60);
      CompetitionDijkstra nullCompetition = new CompetitionDijkstra(null, 50, 50, 50);
      int tinyTime = tinyCompetition.timeRequiredforCompetition();
      int bigTime = bigCompetition.timeRequiredforCompetition();
      int impossibleTime = impossibleCompetition.timeRequiredforCompetition();
      int missingFileTime = missingFileCompetition.timeRequiredforCompetition();
      int badSpeedTime = badSpeedCompetition.timeRequiredforCompetition();
      int nullTime = nullCompetition.timeRequiredforCompetition();
      assertEquals("Checking value for tinyTime", 31, tinyTime);
      assertEquals("Checking value for bigTime", 24, bigTime);
      assertEquals("Checking value for impossibleTime", -1, impossibleTime);
      assertEquals("Checking value for missingFileTime", -1, missingFileTime);
      assertEquals("Checking value for badSpeedTime", -1, badSpeedTime);
      assertEquals("Checking value for nullTime", -1, nullTime);

      
    }

    @Test
    public void testFWConstructor() {
      CompetitionFloydWarshall tinyCompetition = new CompetitionFloydWarshall("tinyEWD.txt", 80, 70, 60);
      CompetitionFloydWarshall bigCompetition = new CompetitionFloydWarshall("1000EWD.txt", 80, 70, 60);
      CompetitionFloydWarshall impossibleCompetition = new CompetitionFloydWarshall("impossibleEWD.txt", 80, 70, 60);
      CompetitionFloydWarshall missingFileCompetition = new CompetitionFloydWarshall("missingEWD.txt", 80, 70, 60);
      CompetitionFloydWarshall badSpeedCompetition = new CompetitionFloydWarshall("tinyEWD.txt", 1000, 70, 60);
      CompetitionFloydWarshall nullCompetition = new CompetitionFloydWarshall(null, 50, 50, 50);
      int tinyTime = tinyCompetition.timeRequiredforCompetition();
      int bigTime = bigCompetition.timeRequiredforCompetition();
      int impossibleTime = impossibleCompetition.timeRequiredforCompetition();
      int missingFileTime = missingFileCompetition.timeRequiredforCompetition();
      int badSpeedTime = badSpeedCompetition.timeRequiredforCompetition();
      int nullTime = nullCompetition.timeRequiredforCompetition();
      assertEquals("Checking value for tinyTime", 31, tinyTime);
      assertEquals("Checking value for bigTime", 24, bigTime);
      assertEquals("Checking value for impossibleTime", -1, impossibleTime);
      assertEquals("Checking value for missingFileTime", -1, missingFileTime);
      assertEquals("Checking value for badSpeedTime", -1, badSpeedTime);
      assertEquals("Checking value for nullTime", -1, nullTime);
    }
    
}
