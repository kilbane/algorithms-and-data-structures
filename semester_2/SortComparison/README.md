# SortComparison
The aims of this exercise were to:
- Implement insertion sort
- Implement selection sort
- Implement quick sort
- Implement merge sort using recursion
- Implement merge sort using iteration
- Ensure 100% code coverage through unit tests
- Analyse and compare the performance of each algorithm for different sized
  inputs

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

With no test failures, the output will be blank.
