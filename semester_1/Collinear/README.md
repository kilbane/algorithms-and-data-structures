# Collinear
The aims of this exercise were to:
- Write the following methods
  - Count the number of non-horizontal lines that go through 3 points in arrays a1, a2, a3 using a brute force algorithm
  - Count the number of non-horizontal lines that go through 3 points in arrays a1, a2, a3 using a more efficient algorithm
  - Sort an array of integers using insertion sort
  - Search for an integer in a sorted array of integers using binary search
- Ensure 100% code coverage through unit tests 
- Calculate the complexity of each method using asymptotic notation

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

No output means that all the tests were passed.
