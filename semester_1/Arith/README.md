# Arith
The aims of this exercise were to:
- Write methods for validating, evaluating and converting between prefix and postfix arithmetic expressions
- Ensure 100% code coverage through unit tests 
- Analyse the complexity of each method using Big O notation

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

No output means that all the tests were passed.
