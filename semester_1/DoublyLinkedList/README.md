# DoublyLinkedList
The aims of this exercise were to:
- Write the following methods
  - Insert an element into a doubly linked list
  - Return the data stored at a particular position in a doubly linked list
  - Delete the data stored at a particular position in a doubly linked list
  - Reverse a doubly linked list
  - Remove all duplicate elements in a doubly linked list
  - Push an element onto a stack
  - Pop an element off of a stack
  - Enqueue an element
  - Dequeue an element
- Ensure 100% code coverage through unit tests 
- Calculate the complexity of each method using asymptotic notation

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

No output means that all the tests were passed.
