# BST
The aims of this exercise were to:
- Write the following methods for a binary search tree
  - get the height
  - get the median key
  - print the keys in order
  - pretty print the keys
  - delete a key
- Ensure 100% code coverage through unit tests 

## To build
`javac -cp .:../../dependencies/* *.java`

## To run
`java -cp .:../../dependencies/* TestRunner`

No output means that all the tests were passed.
